from unittest import TestCase
from source import api
import json

class TestAPI (TestCase):
    def setUP(self):
        self.client = app.test_client()

    def test_host(self):
        result = self.client.get('/')

        self.assertEqual(result.status_code, 200)
        self.assertEqual(result, 'It Works')

    def test_data(self):
        result = self.client.get('/data')

    def test_start_end(self):
        ...

    def test_bridge(self):
        bridge_keys = ['RVW', 'KR', 'MH', 'NB', 'BM']

        for bridge in range(len(bridge_keys)):
            result = self.client.get('/'+bridge_keys[bridge])
            self.assertEqual(result.status_code, 200)

    def test_total_start_end(self):
        ...

    def test_total_bridge(self):
        bridge_keys = ['RVW', 'KR', 'MH', 'NB', 'BM']

        for bridge in range(len(bridge_keys)):            
            result = self.client.get('/total/'+bridge_keys[bridge])
            self.assertEqual(result.status_code, 200)

    def test_mean_startEnd(self):
        ...

    def test_mean_startEnd_bridge(self):
