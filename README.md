# COE 332 Final Project



### Contributors 
Jasmin Lim, Shealyn Greer, and Katharine Fisher



### Project Summary
This time series data set looks at the traffic count for select bridges in New York City from 1933 up to 2017. The bridges included are: Rip Van Winkle Bridge, Kingston-Rhinecliff Bridge, Mid-Hudson Bridge, Newburgh-Beacon Bridge, and Bear Mountain Bridge. This traffic data proves useful for transportation engineers who must analyze traffic flow in order to understand how quickly the bridges are deteriorating and how often they need to undergo maintenance. Additionally, city planners can use the bridge traffic data to analyze the increase or decrease of the population in certain areas. This specific data can be further analyzed to determine whether new roadways need to be opened.


### Endpoints and Outcomes

+ GET /data

		returns traffic data for all five bridges from 1933 to 2017 as well as the total bridge traffic, deviation, and percent
		deviation for each year  



+ GET /<start-year>/<end-year> 

		returns all yearly data for all five bridges beginning at the start year up to but not including the end year



+ GET /<bridge>

		returns all data for a particular bridge from 1933 to 2017. Use abbreviations: RVW - Rip Van Winkle Bridge, 
		KR - Kingston-Rhinecliff Bridge, MH - Mid-Hudson Bridge, NB - Newburgh-Beacon Bridge, BM - Bear Mountain Bridge



+ GET /total/<start-year>/<end-year>

		returns the total bridge traffic from the start year up to and not including the end year



+ GET /total/<bridge>

		returns the total traffic for a bridge. Use abbreviations: RVW - Rip Van Winkle Bridge, KR - Kingston-Rhinecliff Bridge, 
		MH - Mid-Hudson Bridge, NB - Newburgh-Beacon Bridge, BM - Bear Mountain Bridge



+ GET /mean/<start-year>/<end-year>

		returns the mean traffic experienced by a bridge from the start year up to and not including the end year



+ GET /mean/<start-year>/<end-year>/<bridge>

		returns the mean traffic on a bridge from the start year up to and not including the end year. Use abbreviations: 
		RVW - Rip Van Winkle Bridge, KR - Kingston-Rhinecliff Bridge, MH - Mid-Hudson Bridge, NB - Newburgh-Beacon Bridge, BM - Bear Mountain Bridge



+ GET /graph/<start-year>/<end-year>

		returns a .png file with a graph of traffic on each bridge from the start year up to but not including the end year



+ GET /pie/<year>

		returns a .png file with a pie chart comparing traffic on each bridge for the given year



+ GET /roll/<bridge>/<rolling-window>

		returns a .png file with a graph comparing annual traffic on a given bridge to the rolling mean of traffic over a given number of years.
		Use abbreviations: RVW - Rip Van Winkle Bridge, KR - Kingston-Rhinecliff Bridge, MH - Mid-Hudson Bridge, NB - Newburgh-Beacon Bridge, 
		BM - Bear Mountain Bridge 



+ POST /add/<year>

		allows reader to add data on traffic on the five bridges for a new year



### Example Calls



+ With Curl:



```python
		curl http://0.0.0.0:5000/1948/1950
```



```python
		Output:
		
		{
		
			1948:{
			
				"Rip Van Winkle Bridge": 957702,
			
				"Kingston-Rhinecliff Bridge": 0,
			
				"Mid-Hudson Bridge": 2454479,
			
				"Newburgh-Beacon Bridge": 0,
			
				"Bear Mountain Bridge": 1044787,
			
				"Total": 4456968,
			
				"Deviation": 384851,
			
				"Deviation %": 9.45
			
			},
		
			1949:{
		
				"Rip Van Winkle Bridge": 1129075,
			
				"Kingston-Rhinecliff Bridge": 0,
			
				"Mid-Hudson Bridge": 2722172,
			
				"Newburgh-Beacon Bridge": 0,
			
				"Bear Mountain Bridge": 1179561,
			
				"Total": 5030808,
			
				"Deviation": 573840,
			
				"Deviation %": 12.88
			
			},
			
		}
```
			


```python
		curl http://0.0.0.0:5000/total/KR
```



```python
		Output:
		
		{"Kingston-Rhinecliff Bridge, Total": 287332154}
```



```python
		curl http://0.0.0.0:5000/add/2018 -d '{"Rip Van Winkle Bridge": 5942352,"Kingston-Rhinecliff Bridge": 8198068,
		"Mid-Hudson Bridge": 14494120,"Newburgh-Beacon Bridge": 26718598,"Bear Mountain Bridge": 7848404,"Total": 63201542,
		"Deviation": 366708,"Deviation %": 0.58}' -H 'Content-Type: application/json'
```



```python
		Output:
		
		Success! Data posted for the year 2017.
```



```python
		curl http://0.0.0.0:5000/graph/1937/1974
```



```python
		Output:
		
		An image will be returned to you in raw bytes, and a .png file will be saved in source/images.
```



+ With Python:



```python
		import requests

		response = requests.get("http://0.0.0.0:5000/mean/1981/1997")
		print(response.json()) 
```



```python
		Output:
		
		{"Mean, 1981-1997": 8350085.718}
```



```python
		data = {
		
				"Rip Van Winkle Bridge": 5942352,
			
				"Kingston-Rhinecliff Bridge": 8198068,
			
				"Mid-Hudson Bridge": 14494120,
			
				"Newburgh-Beacon Bridge": 26718598,
			
				"Bear Mountain Bridge": 7848404,
			
				"Total": 63201542,
			
				"Deviation": 366708,
			
				"Deviation %": 0.58
			
			}
			
		response = requests.post("http://0.0.0.0:5000/add/2017", data = data)
		print(response.text) 
```



```python
		Output:
		
		Success! Data posted for the year 2017.
```



### Pricing
The method for charging for access to the API could potentially come from 1 of 3 different categories: pay-as-you-go, fixed quota, or overage model. For the purpose of this specific time series data set handling bridge traffic data, it would be best to charge the client based on the overage model. The overage model allows the client a fixed number of calls each month and charges a small overage fee if the client exceeds that specified number of calls. This model allows for predictable pricing and revenue and ensure the app is never shut down. There is a downside to this method as miscommunication around overages could occur. Within this overage model plan, there could be different price levels depending upon who the client is.
