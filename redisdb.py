import redis
import json
import os

#from flask import Flask, jsonify, request

#app = Flask(__name__)

REDIS_IP = os.environ.get('REDIS_IP')
REDIS_PORT = os.environ.get('REDIS_PORT')

r = redis.StrictRedis(host=REDIS_IP, port=6379, db=0)
data = json.load(open('bridge_data.json', 'r'))
length = len(data)
hashKey = []

for i in range(length):
    hashName = str(data[i]["Year"])
    year_data = {"Year": data[i]["Year"], "Rip Van Winkle Bridge": data[i]["Rip Van Winkle Bridge"], "Kingston-Rhinecliff Bridge": data[i]["Kingston-Rhinecliff Bridge"], "Mid-Hudson Bridge": data[i]["Mid-Hudson Bridge"], "Newburgh-Beacon Bridge": data[i]["Newburgh-Beacon Bridge"], "Bear Mountain Bridge": data[i]["Bear Mountain Bridge"], "Total": data[i]["Total"], "Deviation": data[i]["Deviation"], "Deviation %": data[i]["Deviation %"]}
    r.hmset(hashName, year_data)
    hashKey_temp = {}
    hashKey_temp = hashName
    hashKey.append(hashKey_temp)

#@app.route('/')
#def get_data():
#    msg = []
#    for i in range(length):
#        temp_msg = {}
#        temp_msg = r.hgetall(hashKey[i])
#        msg.append(temp_msg)

#    return str(msg)

#@app.route('/RVW')
#def get_RVW():
#    msg = []
#    for i in range(length):
#        temp_msg = {}
#        temp_msg = r.hmget(hashKey[i], 'Year', 'Rip Van Winkle Bridge')
#        msg.append(temp_msg)

#    return str(msg)

#if __name__ == "__main__":
#    app.run()
